from cam_server.utils import timestamp_as_float
from logging import getLogger 
import numpy as np
import json

_logger = getLogger(__name__)

def addGaussSpot(image, sigma_1, sigma_2, mu_1, mu_2, rotAngle=0):
    if not (mu_1 < image.shape[0] and mu_2 < image.shape[1]):
        print ("Spot outside image, igoring...")
        return image
    x = np.arange(image.shape[0])
    y = np.arange(image.shape[1])
    g_x = np.exp(-(x - mu_1)**2/(2.0*sigma_1**2))
    g_y = np.exp(-(y - mu_2)**2/(2.0*sigma_2**2))
    g = np.kron(g_x, g_y).reshape(image.shape)#/(2*np.pi*sigma_1*sigma_2)
    if rotAngle > 0:
        g = ndi.rotate(g, rotAngle, reshape=False)
    return image + g
    
    
def process_image(image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata=None):

    position_row = image.shape[0]//2
    position_colums = image.shape[1]//2
    sigma_row = 12
    sigma_column = 24
    image = np.copy(image)
    image = addGaussSpot(image, sigma_row, sigma_column, position_row, position_colums)

    od = dict()
    od["image"] = image
    od["pulse_id"] = pulse_id
    #_logger.info(str(pulse_id))
    od["timestamp"] = timestamp_as_float(timestamp)
    od["x_axis"] = x_axis
    od["y_axis"] = y_axis
    #od["parameters"] = json.dumps({"camera_name": "simulation"})
    od["parameters"] = json.dumps(parameters)
    
    return od

