import time
import zmq
import pprint

def result_collector():
    context = zmq.Context()
    results_receiver = context.socket(zmq.PULL)
    results_receiver.bind("tcp://127.0.0.1:55558")
    collector_data = {}
    pulse_data = {}
    try:
        while True:
            result = results_receiver.recv_json()
            if not pulse_data: #start timer after receiving
                start_time = time.time()
            worker_id = result['worker']
            pulse_id = result['pulse']
            if worker_id in collector_data:
                collector_data[worker_id] += 1
            else:
                collector_data[worker_id] = 1
            if worker_id in pulse_data:
                pulse_data[worker_id].append(pulse_id)
            else:
                pulse_data[worker_id] = [pulse_id]
            print ("Calculation result:", result['sum'])
            print ("Who did receive how much messages:", collector_data)
            totalReceivedMessages = sum(collector_data.values())
            print("totalReceivedMessages:", totalReceivedMessages)
            receiverFrequency = totalReceivedMessages/(time.time() - start_time)
            print ("receiverFrequency [Hz]:", receiverFrequency)
    except KeyboardInterrupt:
        print ("Who did receive which pulses:", pulse_data)

if __name__ == "__main__":
    result_collector()

