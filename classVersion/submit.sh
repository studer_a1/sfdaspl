#!/bin/bash
#Variables that are handed over via CLA
PIPELINE_NAME=$1
USER_SCRIPT=$2
echo "Activating python environment"
LOAD_PYTHON_ENV="source /opt/gfa/python 3.7"
${LOAD_PYTHON_ENV}
echo "Starting collector..."
python collect.py &
COLLECTOR_PID=$!
echo "Starting first worker on local host.." #Loop back interface might be better than ${HOSTNAME}
LOCAL_COMMAND="python work.py -ch ${HOSTNAME} -p ${PIPELINE_NAME} -s ${USER_SCRIPT} -n 8"
echo "Running on local host: ${LOCAL_COMMAND}"
${LOCAL_COMMAND} &
WORKER_PIDS="$!"
echo "Starting workers on remote hosts.."
declare -a WORKERNODES=("sf-lc7a-d" "sf-lc7a-w" "sf-lc7a-m") #defines the compute cluster (+localhost)
#declare -a WORKERNODES=("sf-lc7a-d")
SOURCE_DIR=$PWD
for WORKERNODE in "${WORKERNODES[@]}" #Start workers according above defined list
do #we use same command as for local host, i.e same number of cores on all nodes
  REMOTE_COMMAND="cd ${SOURCE_DIR}; ${LOAD_PYTHON_ENV}; ${LOCAL_COMMAND}"
  echo "Running on ${WORKERNODE}: ${REMOTE_COMMAND}"
  ssh "${WORKERNODE}" "${REMOTE_COMMAND}" &
  WORKER_PIDS="${WORKER_PIDS} $!"
done
echo "WORKER_PIDS: ${WORKER_PIDS}" #Wait for workers to finish, such that...
#we can't use 'wait -n', since this waits for all child processes to finish, i.e also the collector process
echo "Waiting for workers to finish.."
#We install a signal handler to catch Ctrl+C and propagate to (child) processes 
function kill_workers()
{
  pkill -u ${USER} -c -f "python work.py" #No signal handler in python/multiprocess
  for WORKERNODE in "${WORKERNODES[@]}" 
  do #ssh does not send signal to child processes, hence this brute force approach
    ssh "${WORKERNODE}" "pkill -u ${USER} -c -f \"python work.py\"" 
  done
}
trap kill_workers SIGINT #Activate signal handler
#Start waiting
for WORKER_PID in ${WORKER_PIDS}
do
  wait ${WORKER_PID}
done
echo "All workers finished, shutting down collector.." #...we can stop collecting results
kill $COLLECTOR_PID
wait $COLLECTOR_PID
echo "Finished"

