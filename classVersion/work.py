import argparse

from workerClass import WorkerClass

def parseArgs():
    parser = argparse.ArgumentParser(description='Worker')
    parser.add_argument('-p', '--pipelineInstanceID', default='', help='Name of pipeline instance to connect to')
    parser.add_argument('-s', '--pathToProcessingScript', default='', help='Path to script containing image processing method')
    parser.add_argument('-ch', '--collectorHostname', default='127.0.0.1', help='Collector tcp://host:port to connect to')
    parser.add_argument('-cp', '--collectorPort', default=55558, help='Collector tcp://host:port to connect to', type=int)
    parser.add_argument('-n', '--numberOfProcessesPerNode', default=16, help='Number of cores per node allocated', type=int)
    arguments = parser.parse_args()
    return arguments


if __name__ == "__main__":
    a = parseArgs()
    w = WorkerClass(a.pipelineInstanceID, 
                    a.pathToProcessingScript, 
                    a.collectorHostname, 
                    a.collectorPort, 
                    a.numberOfProcessesPerNode)
    w.postProcessImageStreamInParallel()

