import zmq
import time

class CollectorClass:
    def __init__(self, port=55558):
        self.port = port
        self.receivedDataList = []

    def receiveProcessedResults(self):
        context = zmq.Context()
        inputStream = context.socket(zmq.PULL)
        inputStream.bind('tcp://*:' + str(self.port))
        try:
            while True:
                receivedData = inputStream.recv_pyobj()
                if not self.receivedDataList: #start timer after receiving
                    start_time = time.time()
                self.receivedDataList.append(receivedData)
                totalReceivedMessages = len(self.receivedDataList)
                print("totalReceivedMessages:", totalReceivedMessages)
                receiverFrequency = totalReceivedMessages/(time.time() - start_time)
                print ("receiverFrequency [Hz]:", receiverFrequency)
        except KeyboardInterrupt:
            listOfPulseIDs = [d['pulse_id'] for d in self.receivedDataList]
            #print ("Received data:", self.receivedDataList)
            print ("Received Pulses:", listOfPulseIDs)

