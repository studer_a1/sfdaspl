import zmq
import os
import sys
import json
import numpy as np
from multiprocessing import Process

from cam_server import PipelineClient
from cam_server.utils import get_host_port_from_stream_address
from bsread import source, PULL


#os.environ["OMP_NUM_THREADS"] = "1"

class WorkerClass:

    def __init__(self, pipelineInstanceID, pathToProcessingScript, collectorHostname, collectorPort, numberOfProcessesPerNode):
        self.pipelineInstanceID = pipelineInstanceID
        self.collectorURL = "tcp://" + collectorHostname + ":" + str(collectorPort)
        self.pathToProcessingScript = pathToProcessingScript
        self.numberOfProcessesPerNode = numberOfProcessesPerNode
        self._findStreamHostAndPort()
        self._importProcessingFunction()


    def _findStreamHostAndPort(self):
        client = PipelineClient()
        stream_address = client.get_instance_stream(self.pipelineInstanceID)
        self._stream_host, self._stream_port = get_host_port_from_stream_address(stream_address)


    def _importProcessingFunction(self):
        if not os.path.exists(self.pathToProcessingScript):
            print ("Python script", str(self.pathToProcessingScript), "not found, exiting..")
            sys.exit()
        pathName, scriptName = os.path.split(self.pathToProcessingScript)[0], os.path.split(self.pathToProcessingScript)[1]
        if not scriptName.endswith('.py'):
            print (scriptName, "does not seem to be a python script, exiting..")
            sys.exit()
        sys.path.append(pathName)
        import importlib
        try:
            processingModule = importlib.import_module(os.path.splitext(scriptName)[0])
            self._processingFunction = processingModule.process_image
        except ImportError:
            print ("Python script", str(pathToProcessingScript), "does not contain process_image function, exiting..")
            sys.exit()


    def _postProcessImage(self, image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata=None):
        '''
        return a dict
        '''
        return self._processingFunction(image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata=None)


    def _postProcessImageStream(self, stream_host, stream_port, collectorURL):
        workerPID = os.getpid()
        print ("Starting process", str(workerPID) )
        context = zmq.Context()
        outputStream = context.socket(zmq.PUSH)
        print ("Connecting to collector:", collectorURL)
        outputStream.connect(self.collectorURL)
        print ("Connecting to pipeline:", str(stream_host), str(stream_port))
        with source(host=stream_host, port=stream_port, mode=PULL) as inputStream:
            #for _ in range(100):
            while True:
                message = inputStream.receive()
                pulse_id = message.data.data["pulse_id"].value
                image = message.data.data["image"].value
                #print (image.dtype, imageshape, image.flags) #uint 16 -> 1MP camera @ 100Hz yields 200MBytes/s
                #image = np.copy(image) #original not writable (?) -> corrected in imported function
                timestamp = message.data.data["timestamp"].value
                x_axis = message.data.data["x_axis"].value
                y_axis = message.data.data["y_axis"].value
                parameters = json.loads(message.data.data["parameters"].value)
                try:
                    bsdata = message.data.data["bsdata"].value
                except KeyError:
                    pass
                outputResult = self._postProcessImage(image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata=None)
                outputStream.send_pyobj(outputResult)


    def postProcessImageStreamInParallel(self):
        for _ in range(self.numberOfProcessesPerNode):
            Process(target=self._postProcessImageStream, args=(self._stream_host, self._stream_port, self.collectorURL)).start()

