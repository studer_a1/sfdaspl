import argparse

from collectorClass import CollectorClass

def parseArgs():
    parser = argparse.ArgumentParser(description='Collector')
    parser.add_argument('-p', '--port', default=55558, help='Port to listen for workers')
    arguments = parser.parse_args()
    return arguments

if __name__ == "__main__":
    a = parseArgs()
    c = CollectorClass(a.port)
    c.receiveProcessedResults()

