import time
import zmq
import pprint

def result_collector():
    context = zmq.Context()
    results_receiver = context.socket(zmq.PULL)
    results_receiver.bind("tcp://127.0.0.1:55558")
    pulse_data = []
    try:
        while True:
            result = results_receiver.recv_pyobj()
            if not pulse_data: #start timer after receiving
                start_time = time.time()
            pulse_data.append(result["pulse_id"])
            totalReceivedMessages = len(pulse_data)
            print("totalReceivedMessages:", totalReceivedMessages)
            receiverFrequency = totalReceivedMessages/(time.time() - start_time)
            print ("receiverFrequency [Hz]:", receiverFrequency)
    except KeyboardInterrupt:
        print ("Receive pulses:", pulse_data)

if __name__ == "__main__":
    result_collector()

