import numpy as np
#from skimage.feature import peak_local_max
from scipy.optimize import curve_fit
from time import time

initialized = False
prefix = None
curvature = None

def initialize(parameters, image):
    global initialized, prefix, curvature
    prefix = parameters["camera_name"]+":"

    curv_pars = [6.57905502e-05, -1.06989268e-01, 1.58730770e+02]
    xxx = np.arange(image.shape[0])
    curvature = xxx ** 2 * curv_pars[0] + xxx * curv_pars[1] + curv_pars[2]
    curvature -= np.min(curvature)        
    initialized = True
    

def gauss2D(M, mx, my, sx, sy, amp):
    x, y = M
    den = 2 * np.pi * sx * sy
    exparg = -(((x - mx) / sx) ** 2 + ((y - my) / sy) ** 2) / 2.
    return amp * np.exp(exparg) / den

def process_image(image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata=None):
    if not initialized:
        initialize(parameters, image)
    #ret = processor.process_image(image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata)
    ts = time()
    ret = dict()

    ret["pulse_id"] = pulse_id
    #return ret
    #image = np.array(image) #Remove this copy if pipeline parameter "copy" is set to true or multiprocessed

    Spectrum = evt_list = SPC = SPC_wgt = SPC_gauss = None
    try:
         # 1 Apply Threshold
         low, high = 128, 400  # threshold values
         mask = (image < high) * (image > low)  # image is the single frame from the camera
         mask = np.logical_not(mask)
         #image[mask] = 0 #original image unmutable, hence correct @ ......
     
         # 2 Correct Curvature
         #Do only once in initialization
         image_corr = np.zeros(image.shape)
         for j in range(image.shape[0]):
             image_corr[j] = np.roll(image[j], -int(curvature[j]))
         #...@ here: (mask commutes with roll!)
         #image_corr[mask] = 0
         # 3 Binning
         Spectrum = np.sum(image_corr, axis=0)
         
         #"""
         # 4 SPC
     
         size = 50  # crop size around event
         evt_list = np.array([])
         tot_evt = 0
     
         bounds_low = [int(size * 0.5) - 3, int(size * 0.5) - 3, 0.4, 0.4, 1]
         bounds_high = [int(size * 0.5) + 3, int(size * 0.5) + 3, 5, 5, 1e5]
         xx_c, yy_c = np.meshgrid(np.arange(size), np.arange(size))
     
         xx, yy = np.meshgrid(np.arange(image_corr.shape[1]), np.arange(image_corr.shape[0]))
     
         #coords = peak_local_max(image_corr, min_distance=600)  # Identify location of event
         coords = np.asarray([[image.shape[0]//2, image.shape[1]//2]])
         coords = np.asarray([])
         for e in range(len(coords)):
             tot_evt += 1
     
             y0, x0 = coords.astype(int)[e]
     
             extr = image_corr[(y0 - int(size * 0.5)):(y0 + int(size * 0.5)),
                    (x0 - int(size * 0.5)):(x0 + int(size * 0.5))]  # crop the frame
     
             pars = [int(size * 0.5), int(size * 0.5), 1.5, 1.5, 1e5]
             pars, pcov = curve_fit(gauss2D, np.vstack((xx_c.ravel(), yy_c.ravel())), extr.ravel(), p0=pars, maxfev=1000)#,bounds=(bounds_low, bounds_high))  # fit the event
             print ("Fitting took [ms]", str(1000*(time() - ts )))
             pars[0] = x0 - int(size * 0.5) + pars[0]
             pars[1] = y0 - int(size * 0.5) + pars[1]
     
             evt_list = np.append(evt_list, np.append([int(tot_evt)], pars))
     
         evt_list = np.reshape(evt_list, (tot_evt, 6))
     
         # 5 SPC spectra
         image_SPC = np.zeros(image.shape)
         image_SPC_weighted = np.zeros(image.shape)
         image_gauss = np.zeros(image.shape)
     
         for evt in range(tot_evt):
             if ((evt_list[evt, 3] > 0.7) & (evt_list[evt, 4] > 0.7)):  # filter out fake events
                 cx, cy = evt_list[evt, 2], evt_list[evt, 1]
                 image_SPC_weighted[int(cx), int(cy)] += evt_list[evt, 5]
                 image_SPC[int(cx), int(cy)] += 1
                 pars = evt_list[evt][1:]
                 image_gauss += (gauss2D([xx, yy], *pars))
     
         SPC = np.sum(image_SPC, axis=0)
         SPC_wgt = np.sum(image_SPC_weighted, axis=0)
         SPC_gauss = np.sum(image_gauss, axis=0)  
         #"""
    except Exception as e:
          print("Error in processing function: " + str(e))
    #print ("ALL took [ms]", str(1000*(time() - ts )))           
    #return values
    ret[prefix+"Spectrum"]=Spectrum
    ret[prefix+"evt_list"]=evt_list
    ret[prefix+"SPC"]=SPC
    ret[prefix+"SPC_wgt"]=SPC_wgt
    ret[prefix+"SPC_gauss"]=SPC_gauss
    ret["pulse_id"]=pulse_id 
    return ret

