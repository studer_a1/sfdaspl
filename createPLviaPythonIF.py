from cam_server import PipelineClient
from cam_server.utils import get_host_port_from_stream_address
from bsread import source, SUB

# Initialize the client.
pipeline_client = PipelineClient()

# Setup the pipeline config. Use the simulation camera as the pipeline source.
pipeline_config = {
 "name": "test_alain_100Hz",
 "camera_name": "alain",
 "source_type": "simulation",
 "source": "",
 "frame_rate": 10,
 "size_x": 20,
 "size_y": 20,
 "image_type": "static_beam",
 "function": "test_alain.py",
 "mode": "PUSH",
 "no_client_timeout": 1000
}


# Create a new pipeline with the provided configuration. Stream address in format tcp://hostname:port.
instance_id, pipeline_stream_address = pipeline_client.create_instance_from_config(pipeline_config)

print ("pipeline_stream_address:", pipeline_stream_address)

# Extract the stream hostname and port from the stream address.
pipeline_host, pipeline_port = get_host_port_from_stream_address(pipeline_stream_address)

print ("Started Pipeline:", pipeline_host, pipeline_port)

'''
# Subscribe to the stream.
with source(host=pipeline_host, port=pipeline_port, mode=SUB) as stream:
    # Receive next message.
    data = stream.receive()

image_width = data.data.data["width"].value
image_height = data.data.data["height"].value
image_bytes = data.data.data["image"].value

print("Image size: %d x %d" % (image_width, image_height))
print("Image data: %s" % image_bytes)
'''

