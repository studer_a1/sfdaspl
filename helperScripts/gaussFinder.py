from scipy import ndimage as ndi
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max
from skimage import data, img_as_float
from time import time
import numpy as np

from scipy.optimize import curve_fit

initialized = False
prefix=None
curvature=None


def initialize(image):
   global initialized, prefix, curvature
   #prefix = parameters["camera_name"]+":"

   curv_pars = [6.57905502e-05, -1.06989268e-01, 1.58730770e+02]
   xxx = np.arange(image.shape[0])
   curvature = xxx ** 2 * curv_pars[0] + xxx * curv_pars[1] + curv_pars[2]
   curvature -= np.min(curvature)         
   initialized = True
   
def gauss2D(M, mx, my, sx, sy, amp):
    x, y = M
    den = 2 * np.pi * sx * sy
    exparg = -(((x - mx) / sx) ** 2 + ((y - my) / sy) ** 2) / 2.
    return amp * np.exp(exparg) #/ den

   
def addGaussSpot(image, sigma_1, sigma_2, mu_1, mu_2):
    if not (mu_1 < image.shape[0] and mu_2 < image.shape[1]):
        print ("Spot outside image, igoring...")
        return image
    x = np.arange(image.shape[0])
    y = np.arange(image.shape[1])
    g_x = np.exp(-(x - mu_1)**2/(2.0*sigma_1**2))
    g_y = np.exp(-(y - mu_2)**2/(2.0*sigma_2**2))
    g = np.kron(g_x, g_y).reshape(image.shape)#/(2*np.pi*sigma_1*sigma_2)
    return image + g


image = np.zeros((1000, 1000))
position_row = image.shape[0]//4
position_colums = image.shape[1]//4
sigma_row = 7
sigma_column = 12
image = addGaussSpot(image, sigma_row, sigma_column, position_row, position_colums)
image = addGaussSpot(image, 2*sigma_row, 2*sigma_column, 2*position_row, 2*position_colums)

if not initialized:
    initialize(image)
    ret = dict()

image_corr = np.zeros(image.shape)
for j in range(image.shape[0]):
    image_corr[j] = np.roll(image[j], -int(curvature[j]))

local_size = max(sigma_row, sigma_column)

#xx, yy = np.meshgrid(np.arange(image_corr.shape[1]), np.arange(image_corr.shape[0]))
ts = time()
coords = peak_local_max(image_corr, min_distance=6) 
print ("Peakfinding took [ms]", str(1000*(time() - ts )))
#print (len(coords))


size = 10  # crop size around event
evt_list = np.array([])
tot_evt = 0
     
bounds_low = [int(size * 0.5) - 3, int(size * 0.5) - 3, 0.4, 0.4, 1]
bounds_high = [int(size * 0.5) + 3, int(size * 0.5) + 3, 5, 5, 1e5]
xx_c, yy_c = np.meshgrid(np.arange(size), np.arange(size))

for e in range(len(coords)):
    #print ("event:", e)
    tot_evt += 1
    y0, x0 = coords.astype(int)[e]
    extr = image_corr[(y0 - int(size * 0.5)):(y0 + int(size * 0.5)),
                   (x0 - int(size * 0.5)):(x0 + int(size * 0.5))]  # crop the frame
    pars = [int(size * 0.5), int(size * 0.5), 10, 10, 1]
    print ("Curve fitting with initial guess....:", pars)
    ts = time()
    pars, pcov = curve_fit(gauss2D, np.vstack((xx_c.ravel(), yy_c.ravel())), extr.ravel(), p0=pars)
                                   #bounds=(bounds_low, bounds_high))  # fit the event
    pars[0] = x0 - int(size * 0.5) + pars[0]
    pars[1] = y0 - int(size * 0.5) + pars[1]
    evt_list = np.append(evt_list, np.append([int(tot_evt)], pars))
    print ("...yields parameters", pars)
    print ("Fitting took [ms]", str(1000*(time() - ts )))
evt_list = np.reshape(evt_list, (tot_evt, 6))

#for e in evt_list:    print (e)

fig, axes = plt.subplots(1, 2, figsize=(8, 3), sharex=True, sharey=True)
ax = axes.ravel()
ax[0].imshow(image_corr, cmap=plt.cm.gray)
ax[0].axis('off')
ax[0].set_title('Original')

ax[1].imshow(image_corr, cmap=plt.cm.gray)
ax[1].autoscale(False)
ax[1].plot(coords[:, 1], coords[:, 0], 'r.')
ax[1].axis('off')
ax[1].set_title('Peak local max')

fig.tight_layout()

plt.show()

