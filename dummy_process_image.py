import numpy as np

from userScripts.SATES31 import process_image


def addGaussSpot(image, sigma_1, sigma_2, mu_1, mu_2, rotAngle=0):
    if not (mu_1 < image.shape[0] and mu_2 < image.shape[1]):
        print ("Spot outside image, igoring...")
        return image
    x = np.arange(image.shape[0])
    y = np.arange(image.shape[1])
    g_x = np.exp(-(x - mu_1)**2/(2.0*sigma_1**2))
    g_y = np.exp(-(y - mu_2)**2/(2.0*sigma_2**2))
    g = np.kron(g_x, g_y).reshape(image.shape)#/(2*np.pi*sigma_1*sigma_2)
    if rotAngle > 0:
        g = ndi.rotate(g, rotAngle, reshape=False)
    return image + g


im_dim = 1000
image = np.zeros((im_dim, im_dim))
position_row = image.shape[0]//2
position_colums = image.shape[1]//2
sigma_row = 7
sigma_column = 12
image = addGaussSpot(image, sigma_row, sigma_column, position_row, position_colums)
image*=1000
pulse_id = 1

result = process_image(image, pulse_id)
#print (result)
