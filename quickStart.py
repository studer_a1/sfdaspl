from cam_server import PipelineClient
from cam_server.utils import get_host_port_from_stream_address
from bsread import source, SUB, PULL

import pylab as plt

# Create a pipeline client.
client = PipelineClient()

# Define the camera name you want to read. This should be the same camera you are streaming in screen panel.
camera_name = "test_alain"

# Format of the instance id when screen_panel creates a pipeline.
pipeline_instance_id = camera_name + ""

# Get the stream for the pipelie instance.
stream_address = client.get_instance_stream(pipeline_instance_id)

# Extract the stream host and port from the stream_address.
stream_host, stream_port = get_host_port_from_stream_address(stream_address)

print ("Connect to", str(stream_host), str(stream_port) )
# Open connection to the stream. When exiting the 'with' section, the source disconnects by itself.
with source(host=stream_host, port=stream_port, mode=PULL) as input_stream:
    input_stream.connect()
    for _ in range(10):
        # Read one message.
        message = input_stream.receive()

        # Print out the received stream data - dictionary.
        #print("Dictionary with data:\n", message.data.data)
        print("Pulse ID:", message.data.data["pulse_id"].value)

