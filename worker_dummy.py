from cam_server import PipelineClient
from cam_server.utils import get_host_port_from_stream_address
from bsread import source, SUB, PULL

import time
import zmq
import random
from multiprocessing import Process
#import pylab as plt


#from userScripts.SATES31 import process_image
def processImage(image, pulse_id):
    #time.sleep(1) # do some math here
    return image.sum() 

def findStreamHostAndPort(camera_name="test_alain"):
    client = PipelineClient()
    pipeline_instance_id = camera_name + ""
    stream_address = client.get_instance_stream(pipeline_instance_id)
    stream_host, stream_port = get_host_port_from_stream_address(stream_address)
    return stream_host, stream_port


def worker(stream_host, stream_port):
    context = zmq.Context()
    worker_sender = context.socket(zmq.PUSH)
    worker_sender.connect("tcp://127.0.0.1:55558")
    
    worker_id = random.randrange(1,100)
    with source(host=stream_host, port=stream_port, mode=PULL) as input_stream:
        for _ in range(1000):
            message = input_stream.receive()
            pulse_id = message.data.data["pulse_id"].value
            print("Pulse ID:", pulse_id)
            image = message.data.data["image"].value
            #parameters = message.data.data["parameters"].value
            image_process_result = processImage(image, pulse_id)
            result = { 'worker' : worker_id, 'sum' : str(image_process_result), 'pulse': str(pulse_id)}
            worker_sender.send_json(result)


if __name__ == "__main__":
    stream_host, stream_port = findStreamHostAndPort() 
    print ("Connect to", str(stream_host), str(stream_port) )
    #worker(stream_host, stream_port)
    for _ in range(2):
        Process(target=worker, args=(stream_host, stream_port)).start()

