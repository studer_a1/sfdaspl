from cam_server import PipelineClient
from cam_server.utils import get_host_port_from_stream_address
from bsread import source, SUB, PULL

import time
import zmq
import os
import numpy as np
from multiprocessing import Process

#os.environ["OMP_NUM_THREADS"] = "1"

from userScripts.SATES31 import process_image

def findStreamHostAndPort(pipeline_instance_id):
    client = PipelineClient()
    stream_address = client.get_instance_stream(pipeline_instance_id)
    stream_host, stream_port = get_host_port_from_stream_address(stream_address)
    return stream_host, stream_port


def worker(stream_host, stream_port, collectorURL):
    context = zmq.Context()
    worker_sender = context.socket(zmq.PUSH)
    print ("Connecting to collector:", collectorURL)
    worker_sender.connect(collectorURL)
    workerPID = os.getpid()
    print ("Starting process", str(workerPID) )
    with source(host=stream_host, port=stream_port, mode=PULL) as input_stream:
        for _ in range(100):
            message = input_stream.receive()
            pulse_id = message.data.data["pulse_id"].value
            print("Pulse ID:", pulse_id)
            image = message.data.data["image"].value
            #print (image.dtype) #uint 16, i.e 2 bytes -> 1MP camera @ 100Hz yields 200MBytes/s
            image_c = np.copy(image) #original not writable (?) -> crrected in imported function
            #print (image_c.dtype, image_c.shape, image_c.flags)
            #timestamp = message.data.data["timestamp"].value
            #x_axis = message.data.data["x_axis"].value
            #y_axis = message.data.data["y_axis"].value
            #parameters = message.data.data["parameters"].value
            #bsdata = message.data.data["bsdata"].value
            #image_process_result = process_image(image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata=None)
            image_process_result = process_image(image, pulse_id)
            worker_sender.send_pyobj(image_process_result)


if __name__ == "__main__":
    pipeline_instance_id = "test_alain_100Hz"
    pipeline_instance_id = "test_alain"
    stream_host, stream_port = findStreamHostAndPort(pipeline_instance_id) 
    print ("Connect to", str(stream_host), str(stream_port) )
    collectorURL = "tcp://127.0.0.1:55558"
    #worker(stream_host, stream_port, collectorURL)
    numberOfWorkerProcessesPerNode = 16
    for _ in range(numberOfWorkerProcessesPerNode):
        Process(target=worker, args=(stream_host, stream_port, collectorURL)).start()

