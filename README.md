## Purpose

This repo project contains scripts to parallelize the image processing function defined
[here](https://github.com/paulscherrerinstitute/cam_server#pipeline_configuration).
The fan-in/fan-out design pattern implemented is described 
[here](https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/patterns/pushpull.html).
In the current use case images (raw data) is distributed and derived data is collected. 
This has the advantage that 
1) Code is simple
2) User can use same API as for pipeline scripts (*process_image* for image-wise analysis)
3) Will scale out arbitrarily (with respect to CPU, bottleneck is streaming bandwidth)

Disadvantage

1) No integration in current framework
2) CPUs of DAQ server not used per default (but could be used for lightweight pre-calculations)
3) Not sure about computational infrastructure (on which cluster should this run)

### Background

The architecture of the pipeline as implemented now is such that the parallelization of 
image analysis must be done on a image level, e.g. by a multithreaded computation which works
on tiles of a single image. A simpler and more scalable approach is to parallelize on the
stream level, where many processes receive and analyse a single image (single threaded)

## Install

Just checkout the code:

`$ git clone git@gitlab.psi.ch:studer_a1/sfdaspl.git`

## Run

`$ cd ./sfdaspl/classVersion`

`$ ./submit.sh <pipelineName> <userScript>`

where _pipelineName_ is the name of the streaming pipeline to connect to (must have been started via
e.g. the csm GUI) and _userScript_ labels the (full) path to a python script implementing 
the function with signature
> process_image(image, pulse_id, timestamp, x_axis, y_axis, parameters, bsdata=None)

returning a dict.

An example of a working command line (see [here](https://gitlab.psi.ch/studer_a1/sfdaspl/-/blob/master/userScripts/SATES31.py) for implemented worker function example)

`$ ./submit.sh test_alain ../userScripts/SATES31.py`

This will start a collector process on local host and 8 worker processes on 4 different nodes (each, i.e 32 worker processes sending derived data to the collector waiting for it; each worker processes 100 images and then stops.)



Note that the pipeline config script must return all its input params (but can do additional computations on the image, see for example [here](https://gitlab.psi.ch/studer_a1/sfdaspl/-/blob/master/pipelineScripts/test_alain.py) )


#### Starting CamServer Management console
`$ ssh -Y studer_a1@sf-gw.psi.ch`

`$ ssh -Y sf-lca`

`$ source /opt/gfa/python 3.7`

`$ csm`


## Author

alain.studer@psi.ch


